# Artificial Intelligence Final Project

This project aims to develop a full experiment of data mining using machine learning algorithms. The database used for this purpose was a Titanic dataset that is hosted on [UToronto website]. The titanic dataset gives the values of four categorical attributes for each of the 2201 people on board the Titanic when it struck an iceberg and sank. The attributes are **social class** (first class, second class, third class, or crewmember), **age** (adult or child), **sex**, and **whether or not the person survived**.

### Steps of project

1. **Problem identification**: to observ how the survival variable is related to the other variables.
2. **Pre-processing**: cleaning process, removal of missing values, discretization, etc.
3. **Pattern extraction**: use of some algorithms: *naive-bayes* (probabilistic), *ID3* (symbolic), *perceptron* (connectionist) and *k-means* (clustering).
4. **Postprocessing**: comparative analysis between algorithms.
5. **Use of knowledge**: to describe how the gained knowledge can be used.

[UToronto website]: <http://www.cs.utoronto.ca/~delve/data/titanic/desc.html>
