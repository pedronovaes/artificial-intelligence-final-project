library(rpart)

fit <- rpart(Survived ~ Sex + Age + Class, data = caseTita, method = "class")
fit
plot(fit)
text(fit)

library(rattle)
library(rpart.plot)
library(RColorBrewer)

fancyRpartPlot(fit)